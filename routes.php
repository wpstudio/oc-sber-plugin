<?php

use Illuminate\Http\Request;
use Wpstudio\Sber\Classes\SberCheckout;

Route::post('/sber-checkout', function (Request $request) {

    $sberCheckout = new SberCheckout();

    $sberCheckout->changePaymentState($request);

    return exit();
});
