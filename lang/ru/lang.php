<?php return [
    'plugin' => [
        'name' => 'Sber для Mall',
        'description' => 'Добавляет платежный провайдер Sber в плагин Mall'
    ],
    'settings' => [
        'sber_checkout' => 'Sber',
        'sber_test_mode' => 'Тестовый режим',
        'sber_test_mode_label' => 'Тестовый режим',
        'username' => 'Пользователь',
        'username_label' => 'Пользователь',
        'password' => 'Пароль',
        'password_label' => 'Пароль',
        'set_payed_virtual_order_as_complete' => 'Изменять статус оплаченных виртуальных заказов на "Выполнен"',
    ],
    'messages' => [
        'order_number' => 'Заказ №',
    ]
];
